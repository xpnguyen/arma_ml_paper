import matplotlib, os; matplotlib.rcParams["savefig.directory"] = "."
import numpy as np
import pylab as plt
import sklearn as skl
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor


X = np.load("X.npy")
Y = np.load("Y.npy")
print(X.shape)

x, X_test, y, Y_test = train_test_split(X,Y,test_size=0.2)
X_train, X_cv, Y_train, Y_cv = train_test_split(x,y,test_size=0.2)
del x
del y



#X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.25, random_state=2)
scaler = skl.preprocessing.StandardScaler()
yscaler = skl.preprocessing.PowerTransformer() #StandardScaler()
scaler.fit(X)
yscaler.fit(Y)

X_train_scaled = scaler.transform(X_train)
X_test_scaled = scaler.transform(X_test)
y_train_scaled = yscaler.transform(Y_train)
y_test_scaled = yscaler.transform(Y_test)

mlpr = MLPRegressor(hidden_layer_sizes=(20,20,20,15),
                    activation='tanh',
                    solver='lbfgs',
                    max_iter=2*1600,
                    random_state=2)

mlpr.fit(X_train_scaled, y_train_scaled)

ts = mlpr.score(X_train_scaled, y_train_scaled)
test_score = mlpr.score(X_test_scaled, y_test_scaled)
#validation_score = mlpr.score(scaler.transform(X_cv), yscaler.transform(Y_cv))

# the test score was used to tune the hyper-parameters
# the validation set was used to check for overfitting
