import matplotlib, os; matplotlib.rcParams["savefig.directory"] = "."
import numpy as np
import pylab as plt
import sklearn as skl
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor
from lhc_order_train import train_and_test

lhc_sizes = range(4, 16)
X_train, X_test = [], []
Y_train, Y_test = [], []
test_score, validation_score = [], []

for lhc_size in lhc_sizes:
    print(f"training {lhc_size}")
    filenameX = "fc12_lhc_{}_X.npy".format(lhc_size)
    filenameY = "fc12_lhc_{}_Y.npy".format(lhc_size)
    X = np.load(filenameX)[:, 12:] # skip the raw parameters
    Y = np.load(filenameY)

    x_train, x_test, y_train, y_test = train_test_split(X, Y, test_size=0.25, random_state=2)
    X_train += x_train.tolist()
    X_test += x_test.tolist()
    Y_train += y_train.tolist()
    Y_test += y_test.tolist()

    _, ts, vs = train_and_test(np.array(X_train), np.array(Y_train),
                                   np.array(X_test), np.array(Y_test))
    test_score.append(ts)
    validation_score.append(vs)

sizes = [16, 48, 112, 240, 496, 1008, 2032, 4080, 8176, 16368, 32752, 65520]
plt.semilogx(sizes, test_score, "o-")
plt.semilogx(sizes, validation_score, "o-")
plt.ylabel("Model score []")
plt.xlabel("Number of samples []")
plt.legend(["Training", "Validation"])
plt.show()
print((sizes, test_score, validation_score))
