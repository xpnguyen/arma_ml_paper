import pyDOE
import numpy as np
np.random.seed(12344)
import numpy as np
import dfnlab.DFNBasis as dfnb
import dfnlab.DFNHydro as dfnh
import dfnlab.DFNBoundaryCondition as dfnbc
import dfnlab.DFNMesh as dfnmesh
import dfnlab.DFNFlow as dfnflow




min_logT, max_logT = -3, 3
assert max_logT > min_logT
delta_logT = max_logT - min_logT

lhc_sizes = range(13, 17)

from contextlib import redirect_stdout, contextmanager
import os


@contextmanager
def suppress():
    with open(os.devnull, "w") as null:
        with redirect_stdout(null):
            yield

def run_case(transmissivity_array):
    system = dfnb.System()
    system.buildParallelepiped(L1=1)
    dfn = dfnb.DFN(system)
    dfnb.load_disk_file(dfn, "Ktest.disk")
    dfn.computeIntersections()
    assert len(transmissivity_array) == dfn.nbFractures()
    transmissivities = dfnh.HydraulicProperties(dfn)
    for i in range(dfn.nbFractures()):
        transmissivity = dfnh.HydraulicProperty(transmissivity_array[i])
        transmissivities.setTransmissivity(dfn.getFracture(i), transmissivity)
    conditions = dfnbc.DomainBoundaryCondition()
    conditions.setPermeameterCondition(dfn, 10, 1)
    mesh = dfnmesh.DFNMesh(dfn)
    mesh.setMinimumEdgeLength(0.2)
    mesh.setResolutionGeometry(10)
    mesh.setResolutionIntersection(10)
    mesh.setHydraulicProperties(transmissivities)
    mesh.generateMesh("bamg")
    mesh.setBoundaryCondition(conditions)
    flow = dfnflow.solveDarcyProblem(mesh, conditions)
    Keq = flow.hydraulicConductivity().getValue()
    dqnet = flow.channeling().getValue()
    return Keq, dqnet


for lhc_size in lhc_sizes:
    print("starting case {}".format(lhc_size))
    run_data = []
    filename = "lhc_{}.npy".format(lhc_size)
    hyper_cube = pyDOE.lhs(10, 2**lhc_size)
    # we generate the log and take the real value as input
    for i, inputs in enumerate(hyper_cube):
        T_hyper_cube = 10**((inputs * delta_logT) + min_logT)
        with suppress():
            Keq, dqnet = run_case(T_hyper_cube)
        run_data.append(inputs.tolist() + T_hyper_cube.tolist() + [Keq, dqnet])
        if i>0 and i % 500==0:
            print("{} {}%".format(filename, int(100*i/len(hyper_cube))))
            np.save(filename, run_data)
    np.save(filename, run_data)
    print("finished case {}".format(lhc_size))
