import matplotlib, os; matplotlib.rcParams["savefig.directory"] = "."
import numpy as np
np.random.seed(12344)
import pylab as plt
import sklearn as skl
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor

X = np.load("Xrun_IDEAL_ANFO_800_800_0_1.txt.txt.npy")
Y = np.load("Yrun_IDEAL_ANFO_800_800_0_1.txt.txt.npy")
sizes = [8, 16, 48, 112, 240, 496, 1008, 2032, 4080] # 8176, 16368, 32752, 65520

def train_and_test(X_train, y_train,
                   X_test, y_test, target=0):
    scaler = skl.preprocessing.StandardScaler()
    yscaler = skl.preprocessing.StandardScaler()
    scaler.fit(X_train)
    yscaler.fit(y_train)

    X_train_scaled = scaler.transform(X_train)
    X_test_scaled = scaler.transform(X_test)
    y_train_scaled = yscaler.transform(y_train)
    y_test_scaled = yscaler.transform(y_test)
    hl_size = 15
    mlpr = MLPRegressor(hidden_layer_sizes=(hl_size,hl_size,hl_size),
                        activation='tanh',
                        solver='lbfgs',
                        alpha=1e-4,
                        max_iter=2*1600,
                        random_state=1)
    mlpr.fit(X_train_scaled, y_train_scaled[:,target])
    ts = mlpr.score(X_train_scaled, y_train_scaled[:,target])
    vs = mlpr.score(X_test_scaled, y_test_scaled[:,target])
    return mlpr, ts, vs

if __name__ == "__main__":

    lhc_sizes = range(3, 12)
    X_train, X_test = [], []
    Y_train, Y_test = [], []
    test_score, validation_score = [], []

    indices = np.array(range(len(X)))
    np.random.shuffle(indices)

    for lhc_size in lhc_sizes:
        print(f"training {lhc_size}")
        size = 2**lhc_size
        current = indices[:size]
        indices = indices[size:]
        new_X, new_y = X[current], Y[current]
        x_train, x_test, y_train, y_test = train_test_split(new_X, new_y,
                                                            test_size=0.25, random_state=2)
        X_train += x_train.tolist()
        X_test += x_test.tolist()
        Y_train += y_train.tolist()
        Y_test += y_test.tolist()

        model, ts, vs = train_and_test(np.array(X_train), np.array(Y_train),
                                       np.array(X_test), np.array(Y_test))

        test_score.append(ts)
        validation_score.append(vs)


    plt.semilogx(sizes, test_score, "o-")
    plt.semilogx(sizes, validation_score, "o-")
    plt.ylabel("Model score []")
    plt.xlabel("Number of samples []")
    plt.legend(["Training", "Validation"])
    plt.show()
    print((sizes, test_score, validation_score))
