import matplotlib, os; matplotlib.rcParams["savefig.directory"] = "."
import numpy as np
np.random.seed(12344)
import pylab as plt
import pyDOE
import pickle
with open("raw_dict.pkl", "rb") as f:
    mdata = pickle.load(f)


final_load = []
raw_parameters = []
seconds = 0
for value in mdata.values():
    final_load.append(value["result"]["final_load"])
    raw_parameters.append(value["parameters"]["raw_parameters"] +
                          value["parameters"]["friction_angle_array"] +
                          value["parameters"]["cohesion_array"])
    seconds += value["end_time"] - value["start_time"]

Y = np.array(final_load)
X = np.array(raw_parameters)

np.save("sort_X.npy", X)
np.save("sort_Y.npy", Y)


from scipy.spatial import cKDTree
lhc_raw = X[:,:12]
print("building tree")
tree = cKDTree(lhc_raw)


lhc_sizes = range(4, 16)
for lhc_size in lhc_sizes:
    print(lhc_size)
    filenameX = "fc12_lhc_{}_X.npy".format(lhc_size)
    filenameY = "fc12_lhc_{}_Y.npy".format(lhc_size)
    hyper_cube = pyDOE.lhs(12, 2**lhc_size)
    print("starting query", end="")
    _, indices = tree.query(hyper_cube)
    print("Done")    # sample but do not remove any points for now
    XX = X[indices]
    YY = Y[indices]
    np.save(filenameX, XX)
    np.save(filenameY, YY)
