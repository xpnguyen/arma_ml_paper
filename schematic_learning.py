import matplotlib, os; matplotlib.rcParams["savefig.directory"] = "."
import numpy as np
np.random.seed(12344)
import pylab as plt
import pyDOE

plt.rcParams.update({'font.size': 18})

x = np.logspace(np.log10(4),np.log10(131072),500)
a=0.01
y = 1-np.exp(-a*x)
plt.title("Schematic of Learning Curve")
xx = np.array(np.cumsum([2**(n+1)-4 for n in range(2,16)]))
plt.plot(xx, 1-np.exp(-a*xx), "o", color="blue")
plt.ylabel("Model Quality []")
plt.xlabel("Dataset Size []")
plt.semilogx(x,y)
plt.xlim(1,None)
plt.show()
