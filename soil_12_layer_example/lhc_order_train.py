import matplotlib, os; matplotlib.rcParams["savefig.directory"] = "."
import numpy as np
import pylab as plt
import sklearn as skl
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPRegressor

fc_map = np.poly1d([-1.49603151e+00,  1.83220575e+02, -7.72644152e+03,  1.38367935e+05, -8.78011290e+05])
def map_fric_to_coh(fric):
    if fric==0: return 0
    return fc_map(fric)


def train_and_test(X_train, y_train, X_test, y_test):
    new_feature = np.vectorize(map_fric_to_coh)(X_train[:,:12]) + X_train[:,12:]
    X_train = np.hstack((X_train, new_feature))
    new_feature = np.vectorize(map_fric_to_coh)(X_test[:,:12]) + X_test[:,12:]
    X_test = np.hstack((X_test, new_feature))
    scaler = skl.preprocessing.StandardScaler()
    yscaler = skl.preprocessing.StandardScaler() #PowerTransformer() #StandardScaler()
    scaler.fit(X_train)
    yscaler.fit(y_train.reshape(-1,1))

    X_train_scaled = scaler.transform(X_train)
    X_test_scaled = scaler.transform(X_test)
    y_train_scaled = yscaler.transform(y_train.reshape(-1,1)).ravel()
    y_test_scaled = yscaler.transform(y_test.reshape(-1,1)).ravel()

    mlpr = MLPRegressor(hidden_layer_sizes=(36, 36, 36, 36, 36),
                        activation='tanh',
                        solver='lbfgs',
                        max_iter=2*1600,
                        random_state=2)

    mlpr.fit(X_train_scaled, y_train_scaled)

    ts = mlpr.score(X_train_scaled, y_train_scaled)
    vs = mlpr.score(X_test_scaled, y_test_scaled)

    return mlpr, ts, vs
